<?php

/**
 * @file
 * Base action class.
 */

class Action {
  protected $browser = NULL;

  public function __construct($url, $action) {
    $this->browser = new DrupalBrowser();
    $this->browser->base_url = $url['scheme'] . '://' . $url['host'] . $url['path'];
    $this->browser->base_path = $url['path'];

    // Reseed the random number generator.
    srand(mt_rand());
  }

  public function run() {

  }
}

