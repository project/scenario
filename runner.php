#!/usr/bin/php
<?php
/**
 * @file
 * Scenario command-line runner.
 */

define('ROOT', dirname(__FILE__));

$options = getopt("hn:u:s:n:c:");
$options += array(
  'n' => 10,
  'c' => 1,
  'u' => NULL,
  's' => NULL,
);

if (empty($options['u']) || empty($options['s']) || isset($options['h'])) {
  $script = basename(__FILE__);
  echo <<< EOF
Usage: $script [options]
 -n [number]: number of iterations (defaults to 10)
 -c [currency]: concurrency (defaults to 1)
 -u [url]: base URL to benchmark
 -s [scenario-file]: path to the scenario file

EOF;
  die(1);
}

$url = @parse_url($options['u']);

if (empty($url['host']) || empty($url['scheme']) || empty($url['path']) || !($url['scheme'] == 'http' || $url['scheme'] == 'https')) {
  echo "Unable to parse URL " . $options['u'] . ", please enter an absolute URL.\n";
  die(1);
}

$scenario_file = $options['s'];
$scenario_dir = dirname($scenario_file);

include_once ROOT . '/browser.inc';
include_once ROOT . '/action.inc';

// Import the scenario file, it will define $scenario.
include_once $scenario_file;

if (!isset($scenario)) {
  echo "Invalid scenario file.\n";
  die(1);
}

foreach ($scenario as $action) {
  if (!empty($action['file'])) {
    include_once $scenario_dir . '/' . $action['file'];
  }
  $repetitions = isset($action['repetitions']) ? $action['repetitions'] : 1;
  for ($i = 0; $i < $repetitions; $i++) {
    $actions[] = $action;
  }
}

run_scenario($url, $actions, $options['n'], $options['c']);
die();

//----------------------------------------------------------------------------
// Utility functions.
//----------------------------------------------------------------------------

/**
 * Multi-process runner.
 */
function run_scenario($url, array $original_actions, $iterations, $concurrency) {
  $finished = FALSE;
  $children = array();

  while (!$finished || !empty($children)) {
    $spawned_children = FALSE;

    while (!$finished && count($children) < $concurrency) {
      try {
        if (empty($actions)) {
          if ($iterations > 0) {
            $actions = $original_actions;
            $iterations--;
          }
          else {
            $finished = TRUE;
            break;
          }
        }

        $action = array_pop($actions);

        $pid = pcntl_fork();
        if (!$pid) {
          // This is the child process.
          $class = $action['class'];
          $action_object = new $class($url, $action);
          $action_object->run();
          exit();
        }
        else {
          // Register our new child.
          $children[] = $pid;
          $spawned_children = TRUE;
        }
      }
      catch (Exception $e) {
        echo (string) $e . "\n";
      }
    }

    // Wait for children every 100ms.
    if (!$spawned_children) {
      usleep(100000);
      echo "+";
    }

    // Check if some children finished.
    foreach ($children as $cid => $child) {
      if (pcntl_waitpid($child['pid'], $status, WUNTRACED | WNOHANG)) {
        // This particular child exited.
        unset($children[$cid]);
      }
    }
  }
}
